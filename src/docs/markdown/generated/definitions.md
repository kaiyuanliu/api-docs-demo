## Definitions
### Greeting
|Name|Description|Required|Schema|Default|
|----|----|----|----|----|
|content|The name of the user|true|string||
|id||false|integer (int64)||


### User
|Name|Description|Required|Schema|Default|
|----|----|----|----|----|
|age|The age of user|false|integer (int32)||
|name|The name of user|true|string||


