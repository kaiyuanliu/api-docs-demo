# Hundun RESTful API

## Overview
The Hundun API is organized around REST. Our API is based on MGS Platform Evolution APIs and has resource-oriented URLs. It uses HTTP response code to indicate API errors and built-in HTTP authentication and verbs that are understood by HTTP clients. JSON is returned by all API responses, including errors. So please make sure that Content-Type: application/json header included in the request.

### Version information
Version: 1.0

### Contact information
Contact: Game On Media Ltd.

### URI scheme
Host: localhost:8080
BasePath: /

### Tags

* User Tag: User controller api
* Greeting Tag: Getting controller api


