## Resources
### Greeting Tag

Getting controller api

#### getGreetingss
```
GET /api/v1/greeting
```

##### Parameters
|Type|Name|Description|Required|Schema|Default|
|----|----|----|----|----|----|
|QueryParameter|name|User's name|false|string|Niklas|


##### Responses
|HTTP Code|Description|Schema|
|----|----|----|
|200|OK|Greeting|


##### Consumes

* application/json

##### Produces

* application/json

### User Tag

User controller api

#### Retrieve a user
```
GET /api/v1/users/{userName}
```

##### Parameters
|Type|Name|Description|Required|Schema|Default|
|----|----|----|----|----|----|
|PathParameter|userName|userName|true|string||


##### Responses
|HTTP Code|Description|Schema|
|----|----|----|
|200|OK|User|


##### Consumes

* application/json

##### Produces

* application/json

#### Delete a user
```
DELETE /api/v1/users/{userName}
```

##### Parameters
|Type|Name|Description|Required|Schema|Default|
|----|----|----|----|----|----|
|PathParameter|userName|userName|true|string||


##### Responses
|HTTP Code|Description|Schema|
|----|----|----|
|204|No Content|No Content|


##### Consumes

* application/json

##### Produces

* application/json

