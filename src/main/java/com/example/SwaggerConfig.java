package com.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;
import java.util.HashSet;

import static springfox.documentation.builders.PathSelectors.regex;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Bean
    public Docket newsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
            .apiInfo(apiInfo())
            .select()
            .paths(regex("/api/v1.*"))
            .build()
            .protocols(new HashSet<>(Collections.singletonList("https")))
            .host("hundun.broadwaygaming.com")
            .useDefaultResponseMessages(false);
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("Hundun RESTful API")
            .description("The Hundun API is organized around REST. " +
                "Our API is based on MGS Platform Evolution APIs and has resource-oriented URLs. " +
                "It uses HTTP response code to indicate API errors and built-in HTTP authentication and " +
                "verbs that are understood by HTTP clients. JSON is returned by all API responses, including " +
                "errors. So please make sure that Content-Type: application/json header included in the request.")
            .contact(contact())
            .version("1.0")
            .build();
    }

    private Contact contact() {
        return new Contact("Game On Media Ltd.", "http://www.gameonmedia.ie", "");
    }
}
