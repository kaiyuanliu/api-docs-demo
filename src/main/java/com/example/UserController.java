package com.example;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

/**
 * Created by liu.k on 19/04/2016.
 */
@Api(tags = "User Tag", value = "/api/v1/users", description = "User controller api")
@RestController
@RequestMapping(value = "/api/v1/users",
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "Retrieve a user")
    @RequestMapping(value = "/{userName}", method = RequestMethod.GET)
    public User retrieveUser(@PathVariable String userName) {
        return userService.retrieveUser(userName, 22);
    }

    @ApiOperation(value = "Delete a user")
    @RequestMapping(value = "/{userName}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable String userName) {
        User user = new User(userName);
        user.setAge(23);
    }
}
