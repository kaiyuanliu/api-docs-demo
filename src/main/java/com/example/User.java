package com.example;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by liu.k on 19/04/2016.
 */
public class User {
    @ApiModelProperty(notes = "The name of user", required = true)
    private String name;
    @ApiModelProperty(notes = "The age of user")
    private int age;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
            "name='" + name + '\'' +
            ", age=" + age +
            '}';
    }
}
