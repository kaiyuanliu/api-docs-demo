package com.example;

import io.swagger.annotations.ApiModelProperty;

/**
 * Created by liu.k on 12/04/2016.
 */
public class Greeting {

    private final long id;
    private final String content;

    public Greeting(long id, String content) {
        this.id = id;
        this.content = content;
    }

    public long getId() {
        return id;
    }

    @ApiModelProperty(notes = "The name of the user", required = true)
    public String getContent() {
        return content;
    }
}