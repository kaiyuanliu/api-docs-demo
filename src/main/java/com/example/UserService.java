package com.example;

import org.springframework.stereotype.Service;

/**
 * Created by liu.k on 21/04/2016.
 */
@Service
public class UserService {

    public User retrieveUser(String userName, int age) {
        User user = new User(userName);
        user.setAge(age);
        return user;
    }
}
