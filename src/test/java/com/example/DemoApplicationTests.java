package com.example;

import io.github.robwin.markup.builder.MarkupLanguage;
import io.github.robwin.swagger2markup.GroupBy;
import io.github.robwin.swagger2markup.Swagger2MarkupConverter;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import springfox.documentation.staticdocs.Swagger2MarkupResultHandler;
import springfox.documentation.staticdocs.SwaggerResultHandler;

import java.io.File;
import java.util.Arrays;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = DemoApplication.class)
@IntegrationTest
@WebAppConfiguration
public class DemoApplicationTests {
    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mvc;

    @Before
    public void setUp() {
        mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void convertRemoteSwaggerToAsciiDoc() throws Exception {
        // Remote Swagger source
        // Default is AsciiDoc

        String outputDir = System.getProperty("swagger.output.directory");
        mvc.perform(get("/v2/api-docs").accept(MediaType.APPLICATION_JSON))
            .andDo(SwaggerResultHandler.outputDirectory(outputDir).build())
            .andExpect(status().isOk());


//        System.out.println("example dir: " + exampleDir);
//        Swagger2MarkupConverter.from("http://localhost:8080/v2/api-docs")
//            .withPathsGroupedBy(GroupBy.TAGS)
//            .withExamples(exampleDir)
//            .build()
//            .intoFolder(outputDir);

        // Then validate that three AsciiDoc files have been created
//        String[] files = new File("src/docs/asciidoc/generated").list();
//        System.out.println(files);
//        assertThat(files).hasSize(3)
//            .containsAll(Arrays.asList("definitions.adoc", "overview.adoc", "paths.adoc"));
    }

//    @Test
//    public void convertRemoteSwaggerToMarkdown() throws Exception {
//        // Remote Swagger source
//        // Markdown
//        Swagger2MarkupConverter.from("http://localhost:8080/v2/api-docs")
//            .withPathsGroupedBy(GroupBy.TAGS)
//            .withMarkupLanguage(MarkupLanguage.MARKDOWN)
//            .withSeparatedDefinitions()
//            .withExamples("target/generated-snippets")
//            .build()
//            .intoFolder("src/docs/markdown/generated");
//
//        // Then validate that three Markdown files have been created
//        String[] files = new File("src/docs/markdown/generated").list();
//        System.out.println(files);
////        assertThat(files).hasSize(3)
////            .containsAll(Arrays.asList("definitions.md", "overview.md", "paths.md"));
//    }


}
