package com.example;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.servlet.RequestDispatcher;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.restdocs.payload.PayloadDocumentation.fieldWithPath;
import static org.springframework.restdocs.payload.PayloadDocumentation.responseFields;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by liu.k on 19/04/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DemoApplication.class})
public class UserControllerTest {
    private MockMvc mvc;

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    @Rule
    public JUnitRestDocumentation restDocumentation = new JUnitRestDocumentation("target/generated-snippets");

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.standaloneSetup(userController)
            .apply(documentationConfiguration(restDocumentation))
            .build();
    }

//    @Test
//    public void errorExample() throws Exception {
//        mvc.perform(get("/error")
//                        .requestAttr(RequestDispatcher.ERROR_STATUS_CODE, 400)
//                        .requestAttr(RequestDispatcher.ERROR_REQUEST_URI, "/test")
//                        .requestAttr(RequestDispatcher.ERROR_MESSAGE, "The tag 'http://localhost:8080/tags/123' does not exist"))
//            .andDo(print()).andExpect(status().isBadRequest())
//            .andExpect(jsonPath("error", is("Bad Request")))
//            .andExpect(jsonPath("timestamp", is(notNullValue())))
//            .andExpect(jsonPath("status", is(400)))
//            .andExpect(jsonPath("path", is(notNullValue())))
//            .andDo(document("error",
//                responseFields(
//                    fieldWithPath("error").description("The HTTP error that occurred, e.g. `Bad Request`"),
//                    fieldWithPath("message").description("A description of the cause of the error"),
//                    fieldWithPath("path").description("The path to which the request was made"),
//                    fieldWithPath("status").description("The HTTP status code, e.g. `400`"),
//                    fieldWithPath("timestamp").description("The time, in milliseconds, at which the error occurred")
//                )));
//    }

    @Test
    public void retrieveUserExample() throws Exception {
        String userName = "John";
        User user = new User(userName);
        user.setAge(25);
        when(userService.retrieveUser(eq(userName), anyInt())).thenReturn(user);
        mvc.perform(get("/api/v1/users/{userName}", userName)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON))
            .andDo(print()).andExpect(status().isOk())
            .andDo(document("retrieve_a_user", preprocessResponse(prettyPrint())));
    }

}
